﻿#include <iostream>
#include <vector>
#include <fstream>

#define COUNT			100
#define ORGINAL_FILE	"docs/examples/original"
#define WORK_FILE		"docs/examples/c"
#define A_FILE			"docs/examples/a"
#define B_FILE			"docs/examples/b"
#define DATABASE_PATH	"docs/examples/database.dat"
#define INDEX_PATH		"docs/examples/index.idx"

#include "Btree.hpp"
#include "BTreePrinter.hpp"
#include "Function.hpp"


int main()
{
	BTree tree(5);
	BTreePrinter printer;

	int ar[] = { 30, 70,10, 15,5,7,12,13,17,20,21,35,50,32,34, 36,40,42,45,52,60 };

	for (int i = 0; i < 40; i++)
	{
		tree.insert(i);
	}
	printer.print(tree);
	int ar1[] = { 10, 100, 1000, 10000, 100000 };
	//for (auto i : ar1)
	//{
	//	std::ofstream file("docs\\examples\\outCommand"+to_string(i)+".txt", std::fstream::binary);
	//	std::string temp =generateRandomOperations<char>({ 'D', 'A' }, i);
	//	file.write(temp.c_str(), temp.length());
	//	file.close();
	//}
	interprete();
	return 0;
}